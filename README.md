<!-- Improved compatibility of back to top link: See: https://github.com/othneildrew/Best-README-Template/pull/73 -->
<!--
*** Thanks for checking out the Best-README-Template. If you have a suggestion
*** that would make this better, please fork the repo and create a pull request
*** or simply open an issue with the tag "enhancement".
*** Don't forget to give the project a star!
*** Thanks again! Now go create something AMAZING! :D
-->

<!-- PROJECT SHIELDS --> 
<!--
*** I'm using markdown "reference style" links for readability.
*** Reference links are enclosed in brackets [ ] instead of parentheses ( ).
*** See the bottom of this document for the declaration of the reference variables
*** for contributors-url, forks-url, etc. This is an optional, concise syntax you may use.
*** https://www.markdownguide.org/basic-syntax/#reference-style-links
-->
<a name="readme-top"></a>
[![pipeline status](https://gitlab.com/alyssa.yours/blueandromeda/badges/main/pipeline.svg)](https://gitlab.com/alyssa.yours/blueandromeda/develop) [![pipeline status](https://gitlab.com/alyssa.yours/blueandromeda/badges/main/pipeline.svg)](https://gitlab.com/alyssa.yours/blueandromeda/test) [![pipeline status](https://gitlab.com/alyssa.yours/blueandromeda/badges/main/pipeline.svg)](https://gitlab.com/alyssa.yours/blueandromeda/main)
[![pipeline status](https://gitlab.com/alyssa.yours/blueandromeda/badges/develop/coverage.svg)](https://gitlab.com/alyssa.yours/blueandromeda/develop) [![pipeline status](https://gitlab.com/alyssa.yours/blueandromeda/badges/test/coverage.svg)](https://gitlab.com/alyssa.yours/blueandromeda/test) [![pipeline status](https://gitlab.com/alyssa.yours/blueandromeda/badges/main/coverage.svg)](https://gitlab.com/alyssa.yours/blueandromeda/main)


<!--
[![Stargazers][stars-shield]][stars-url]
[![Forks][forks-shield]][forks-url]https://gitlab.com/username/userproject/badges/master/.svg
[![Issues][issues-shield]][issues-url]
[![MIT License][license-shield]][license-url]
[![LinkedIn][linkedin-shield]][linkedin-url]
-->


<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a href="https://gitlab.com/alyssa.yours/blueandromeda">
    <img src="doc/images/logo.png" alt="Logo" width="80" height="80">
    <img src="doc/images/your-andromeda-profile.png" alt="Logo" width="80" height="80">
  </a>

<h3 align="center">BlueAndromeda</h3>
  <p align="center">
    Telegram Bot powered, unofficial [redacted] community project
    <br/><sub><a href="https://letslearnslang.com/origin-of-the-name-andromeda/"><i>'Andro-meda[n] - Ανδρομέδα - ruler of men'</i></a></sub>
    <br/>
    <br/>
    <a href="https://gitlab.com/alyssa.yours/blueandromeda"><strong><u>Explore the docs</u> »</strong></a>
    <br/>
    <a href="https://t.me/your_andromeda_bot">Visit @[redacted]_bot</a>
    ··
    <a href="https://t.me/+4_EqSGS4QMllZWEy">Visit Telegram Community</a>
    ··
    <a href="https://gitlab.com/alyssa.yours/blueandromeda/issues">Report Bug</a>
    ··
    <a href="https://gitlab.com/alyssa.yours/blueandromeda/issues">Request Feature</a>
  </p>
</div>

<!-- TABLE OF CONTENTS -->
<details>
  <summary>Table of Contents</summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a>
      <ul>
        <li><a href="#get-started">Get started</a></li>
        <li><a href="#community-development">Community Development</a></li>
        <a href="#how-to">How-to</a>
        <ul>
          <li><a href="#command-guide">Command Guide</a></li>
          <li><a href="#menu-guide">Menu Guide</a></li>
        </ul>
        <li><a href="#built-with">Built With</a></li>
      </ul>
    </li>
    <li>
      <a href="#developer-getting-started">Developer Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
        <li><a href="#directory-structure">Directory Structure</a></li>
      </ul>
    </li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>
    <li><a href="#acknowledgments">Acknowledgments</a></li>
  </ol>
</details>

---

<!-- ABOUT THE PROJECT -->

## About The Project
<div align="center">
<h3 align="center">· · · · \< \< \< \< · B E T A · A C C E S S · >/ >/ >/ >/ · · · ·</h3>

<p>
  <em>this third-party .NET project offers a convinient [redacted] experience from within your Telegram chat</em>
</p>
</div>

[![project-header][project-header]](https://example.com)

<div align="center">
  <h3 align="center">· · [redacted] // Control // Automate // Play · ·</h3>
</div>

<p>

  [<img src="doc/gifs/shocker-remote.gif" width="200em"/>](doc/gifs/shocker-remote.gif) [<img src="doc/gifs/shocker-modes.gif" width="200em"/>](doc/gifs/shocker-modes.gif) [<img src="doc/gifs/multiplayer-beta-rand.gif" width="117em"/>](doc/gifs/multiplayer-beta-rand) [<img src="doc/gifs/andromeda-control.gif" width="165em"/>](doc/gifs/andromeda-control.gif) [<img src="doc/gifs/tic-tac-toe.gif" width="231em"/>](doc/gifs/tic-tac-toe.gif) [<img src="doc/images/quiz-time.png" width="158em"/>](doc/images/quiz-time.png) <!-- [<img src="doc/images/randomshock.png" width="251em"/>](doc/images/randomshock.png) -->
</p>

### // Get started

> ><small>**NOTE** > > > > **beta product**
> <em>BlueAndromeda was born · 2023-12-03<br/>
> Sometimes she may not behave like you expect in this early stage, dont hesitate retrying, you cant break anything.<br/>
> if you face any issues please <a href="#contact">dont hesitate to contact</a> and your issue will be quickly resolved</em></small>

1. Open Telegram 
   * <a href="https://telegram.org/apps">either download the Telegram app for mobile Android or IOS</a>
   * <a href="https://web.telegram.org/">or visit the Telegram webapp on your desktop browser</a>
2. Chat with Andromeda users and have fun interacting with their menus<br/>
3. <em>(optional)</em> Setup your [redacted] devices

   1. start a conversation with <a href="https://t.me/your_andromeda_bot">@your_andromeda_bot</a>
   2. follow bot instructions to register an Andromeda account and link it to your [redacted] account<br/>
   <small>requires 'Username', 'ShareCode' and <a href="https://pishock.com/#/account">[redacted].com > Account > 'API-key'</a></small>

4. <em>(optional)</em> Get in touch with others and visit one of the Telegram groups
   for example:
   * <a href="https://t.me/+4_EqSGS4QMllZWEy">[redacted] testing group for [redacted]</a></br>
5. Visit the <a href="#how-to">how-to guide</a>  section for an explenation of available features and menu´s


<div align="center">
<p><small>
❤️ <em><a href="#contact">if you face <strong>any</strong> issues, please contact</a></em> ❤️<br/>
❤️ <em><a href="https://gitlab.com/alyssa.yours/BlueAndromeda/-/issues">open an 'issue' for community discussion</a></em> ❤️
</small></p>
</div>


<div align="right">(<a href="#readme-top">back to top</a>)</div>

<!-- 
  <p float="left">
      <img src="doc/gifs/shocker-remote.gif" width="250em" alt="Girl in a jacket" />
      <img src="doc/gifs/tic-tac-toe.gif" width="300em" alt="Girl in a jacket" />
      <br/>
      <sub>your spawnable virtual shocker remote ···· an evergrowing list of community developed features</sub>
  </p>

<small>virtual shocker remote</small>         |  <small>community developed playground</small>
:-------------------------:|:-------------------------:  
[<img src="doc/gifs/shocker-remote.gif" width="200em"/>](doc/gifs/shocker-remote.gif)  |  [<img src="doc/gifs/tic-tac-toe.gif" width="250em"/>](doc/gifs/tic-tac-toe.gif)
<small><strong>virtual shocker remote</strong></small>         |  <small><strong>community developed playground</strong></small>
[<img src="doc/gifs/shocker-remote.gif" width="200em"/>](doc/gifs/shocker-remote.gif)  |  [<img src="doc/gifs/tic-tac-toe.gif" width="250em"/>](doc/gifs/tic-tac-toe.gif)


<center><em>[<a href="#contributing">visit: 'Contributing' to learn how</a>]</em></center> 
-->
---
<div align="center">
<p>
  <em>got interesting ideas yourself?</em>
</p>
</div>

![community-development-header][community-development-header]
### Community Development

<p>

Focus on open-source community collaboration during development should enable anyone with rudimentary development skills to merge own ideas into the <a href="https://t.me/your_andromeda_bot">project hosted bot</a> or to fork and host an individual version of this project.

<small>

> - very easy template style implementation of new features in a collaborative way
> - leverages Telegram Bot reply-markups to provide chat inline UI/UX capabilities 
> - SOTA dependency-injection automatically hands your implementation anything it needs to function
> - add new factories, models or options to create, contain and persist your data
>   - reference implementations of others to utilize or build upon them yourself in a byref-shared or byval-isolated way
> - inherit new command-, callback-, menu-, lobby- or event- strategy childs to add your own UI and ideas 
> - inline-menus are stateful and provided as persisted finite-state-machines, each provides arcade-machine like capabilities
> - Telegram has the ability to inline inject JavaScript games, which offer their results to the bot <em>(https://core.telegram.org/bots/games)</em>


</small>
<img src="doc/images/strategy-inheritance.png" width="1240em"/>

<div align="center">
<em>[<a href="#contributing">Strategy-Inheritance; Visit: 'Contributing' to add your own</a>]</em>
</div>
</p>

<!-- Here's a blank template to get started: To avoid retyping too much info. Do a search and replace with your text editor for the following: `code-red-team`, `blueandromeda`, `twitter_handle`, `linkedin_username`, `email_client`, `email`, `project_title`, `project_description` -->

<div align="right">(<a href="#readme-top">back to top</a>)</div>

---

### How-to

#### Command Guide

| Command            | Type    | Explenation                                                                                                   |
|--------------------|---------|---------------------------------------------------------------------------------------------------------------|
| /registeruser      | <em>account</em> | Registeres a new user with the bot. Overrides existing user                                                   |
| /registersharecode | <em>account</em> | Registeres a new sharecode with the bot. Overrides existing sharecode                                         |
| /shockermenu       | <em>menu</em>    | Spawns a toggle-able, shocker-remote menu which lets others control the spawning users devices and offers advanced settings                      |
| /andromedacontrol  | <em>lobby</em>   | Andromeda controls joined user devices for up to 20 minutes. <br/><small>(customize experience in /shockermenu > settings)</small>              |
| /randomshock       | <em>lobby</em>   | Each round a coinflip for each user decides who gets shocked. <br/><small>(customize experience in /shockermenu > settings)</small>           |
| /tictactoe         | <em>lobby</em>   | The looser of the round gets shocked. <br/><small>(customize experience in /shockermenu > settings)</small>                        |
| /exit              | <em>utility</em> | Exits a transaction in which Andromeda waits for a send message by the user. For example during /registeruser |
| <small><em>\<add-your-own></em></small>     |         | <small><em>Inherit solutions <a href="">BaseCommandStrategy</a> to start collaboration and add your own command</em></small>                                                                                                          
| <small><em>\<add-your-own></em></small>     |         | <small><em>Inherit solutions <a href="">BaseMenuStrategy</a> to add your own menu implementation</em></small>                                                                                                          
| <small><em>\<add-your-own></em></small>     |         | <small><em>Inherit solutions <a href="">BaseLobbyStrategy</a> to add your own lobby implementation</em></small>                                                                                                          

<div align="right">(<a href="#readme-top">back to top</a>)</div>

#### Menu Guide
| Menu            | Explenation                                                                                                     |
|--------------------|------------------------------------------------------------------------------------------------------------------------|
<img src="doc/images/how-to-shocker-remote.png"/> | <strong>/shockermenu</strong><br/>1) select duration then intensity to shock the menu owner<br/>2) shock menu owner, andromeda decides how<br/>3) vibrate menu owner, andromeda decides how<br/>4) beep menu owner, andromeda decides how<br/>5) beep menu owner for a second (attention)<br/>6) toggle menu function on/off (only accesible by menu owner)<br/>7) set limits and customize andromedas decision making (only accesible by menu owner)
<img src="doc/images/how-to-shocker-remote-settings.png"/> | <strong>/shockermenu > Settings</strong><br/>1) set your global duration floor and ceiling limits<br/>2) set your global intensity floor and ceiling limits<br/>3) set duration/intensity mode Andromeda uses for her decision making<br/>4) toggle your devices beeping, vibratingor shocking on/off; vibration gets used as fallback if not as well disbled
<img src="doc/images/how-to-fixed-lobby.png"/> | fixed lobby size example: <strong>/andromedacontrol or /tictactoe</strong><br/>1) Join the lobby<br/>2) delete the menu message from chat<br/>3) Lobby status indicator, displays "GO" when lobby is full
<img src="doc/images/how-to-variable-lobby.png"/> | variable lobby size example: <strong>/randomshock</strong><br/>1) Player one of the lobby<br/>2) Player two of the lobby <small>(bots get added if an already part of the lobby player taps "join" again, kick bot by tapping it)</small><br/>3) Decrease player size of the lobby<br/>4) Increase player size of the lobby<br/>5) Launch / Start (displays "Play Again" when available)

<div align="right">(<a href="#readme-top">back to top</a>)</div>

---

### Built With

<div align="center">
<p><em><a href="https://learn.microsoft.com/en-us/dotnet/core/whats-new/dotnet-8">.NET8</a> · <a href="https://www.nuget.org/packages/Telegram.Bot">Telegram.Bot</a> · <a href="https://pishock.com/">PiShock API</a> · <a href="https://serilog.net/">Serilog</a> · <a href="https://swagger.io/">Swagger</a></em></p>
</div>

Solution uses current <a href="https://learn.microsoft.com/en-us/aspnet/core/fundamentals/host/generic-host?view=aspnetcore-8.0"> .NET Generic Host</a> 'virtual service container abstraction' architecture and dependency injection standard for modular microservice oriented solution designs; in platform independent and containerized runtime environments.

<strong>'Industry-Standard' implementations for</strong>
- <a href="https://learn.microsoft.com/en-us/aspnet/core/fundamentals/host/generic-host?view=aspnetcore-8.0"> .NET Generic Host</a> and <a href="https://learn.microsoft.com/en-us/dotnet/api/microsoft.extensions.hosting.ihostapplicationlifetime?view=dotnet-plat-ext-8.0">IHostApplicationLifetime</a> <small><em><< base of the current .NET  solution architecture standard</em></small>
- <a href="https://learn.microsoft.com/en-us/dotnet/api/microsoft.extensions.logging.ilogger?view=dotnet-plat-ext-8.0">.NET ILogger\<T> <= Serilog</a> <small><em><< dont write your own logging logic, MS and others did it for you</em>❤</small>
- <a href="https://learn.microsoft.com/en-us/aspnet/core/fundamentals/configuration/options?view=aspnetcore-8.0">.NET Options Pattern | IOptions\<T></a> <small><em><< dont write your own file or appsettings logic, M$</em>❤</small>
- <a href="https://refactoring.guru/design-patterns/strategy">Strategy Pattern</a> <small><em><< dynamic resolution of services and behavior for task execution</em></small>
- <a href="https://refactoring.guru/design-patterns/composite">Composite Pattern</a> <small><em><< no duplicate code</em></small>
- <a href="https://learn.microsoft.com/en-us/dotnet/csharp/fundamentals/object-oriented/polymorphism">Polymorhpism</a>  <small><em><< no duplicate code</em></small>
 

<strong>Noteworthy Dependencies</strong>
<p>

> * <a href="https://www.microsoft.com/">Microsoft</a>
>   * <a href="https://learn.microsoft.com/en-us/dotnet/api/microsoft.extensions.logging?view=dotnet-plat-ext-8.0">Microsoft.Extensions.Logging</a>
>   * <a href="https://learn.microsoft.com/en-us/dotnet/api/microsoft.extensions.configuration?view=dotnet-plat-ext-8.0">Microsoft.Extensions.Configuration</a>
>   * <a href="https://learn.microsoft.com/en-us/dotnet/api/microsoft.extensions.options?view=dotnet-plat-ext-8.0">Microsoft.Extensions.Options</a>
>   * <a href="https://learn.microsoft.com/en-us/dotnet/api/microsoft.extensions.dependencyinjection?view=dotnet-plat-ext-8.0">Microsoft.Extensions.DependencyInjection</a>
>   * <a href="https://learn.microsoft.com/en-us/dotnet/api/microsoft.extensions.hosting?view=dotnet-plat-ext-8.0">Microsoft.Extensions.Hosting</a>
>     * <a href="https://learn.microsoft.com/en-us/dotnet/api/microsoft.extensions.hosting.hostingabstractionshostextensions?view=dotnet-plat-ext-8.0">Microsoft.Extensions.Hosting.Abstractions</a>
> * <a href="https://serilog.net/">Serilog</a>
>   * Serilog.Extensions.Hosting
>   * Serilog.Settings.Configuration
> * <a href="https://swagger.io/">Swagger</a>
>   * Swashbuckle.AspNetCore
> * <a href="https://core.telegram.org/bots/api">Telegram Bot API</a>
>   * <a href="https://www.nuget.org/packages/Telegram.Bot">Third party library Telegram.Bot</a>
>.  
>
> * My favourite collection type for event streaming:
>   * <a href="https://learn.microsoft.com/en-us/dotnet/standard/collections/thread-safe/blockingcollection-overview">BlockingCollection\<T>
>     * System.Collections.Concurrent.IProducerConsumerCollection\<T></a>

</p>
<div align="right">(<a href="#readme-top">back to top</a>)</div>


<!-- GETTING STARTED -->
## Developer Getting Started


### Prerequisites

Prerequisites for when you want to build and host your own version of this bot:
* .NET8
  - <a href="https://github.com/dotnet/core/blob/main/release-notes/8.0/install-windows.md">Windows Installation Instructions</a>
  - <a href="https://github.com/dotnet/core/blob/main/release-notes/8.0/install-linux.md">Linux Installation Instructions</a>
  - <a href="https://github.com/dotnet/core/blob/main/release-notes/8.0/install-macos.md">Macos Installation Instructions</a>
  <br/>
* Your own Telegram Bot and associated Token
  - <a href="https://core.telegram.org/bots">Telegram Bot Guide</a>
    - <a href="https://t.me/botfather">BotFather on Telegram</a>

<div align="right">(<a href="#readme-top">back to top</a>)</div>

### Installation

1. Clone the repo
   ```sh
   git clone https://github.com/alyssa.yours/blueandromeda.git
   ```
1. Enter your app name in `appsettings.json`
   ```js
   "app": {
     "applicationKey": "YOUR_APP_NAME"
   },
   ```
1. Enter the Token of your: 
   * DEV bot in `./src/appsettings.Development.json`
   * TEST bot in `./src/appsettings.Test.json`
   * PROD bot in `./src/appsettings.Production.json`
   ```js
   "BotOptions": {
     "Token": "YOUR_TOKEN",
     "Commands": [
      ...
   ```
1. Build BlueAndromeda.Api solution (use Debug or Release build config)
   ```sh
   RUN dotnet build "./src/BlueAndromeda.Api.csproj" -c Release -o /app/build
   ```

1. Execute BlueAndromeda.Api.exe for quick and simple CLI launch and std::out logs; .dll for primary hosting
    ```batch
    cd ./app/build/
    ```
1. Check logs
    ```batch
    cd ./logs/
    ```

<div align="right">(<a href="#readme-top">back to top</a>)</div>

### Directory Structure

```
├───doc
│   ├───gifs
│   └───images
└───src
    ├───BlueAndromeda                 -- service core logic
    │   ├───Extensions                    -- existing member extensions
    │   ├───Factories                     -- outsourced initialization of repeatingly appearing complex objects
    │   ├───Interfaces
    │   │   ├───ICallbackStrategies 
    │   │   ├───ICommandStrategies        
    │   │   └───IEventStrategies          
    │   ├───Models                        -- data containment implementations
    │   ├───Options                       -- options pattern implementations
    │   ├───Services                      -- BlueAndromeda hosted-services; work (Tele.Message-, Tele.Callback-, BAndro.Event- stream processing)
    │   ├───Strategies                    -- strategy pattern impolementatis
    │   │   ├───CallbackStrategies        -- execution logic for clicked menu buttons
    │   │   ├───CommandStrategies         -- execution logic for /command triggers
    │   │   │   └───MenuStrategies        -- base for menu implementaions
    │   │   │       └───LobbyStrategies   -- base for lobby gatekept game or alike implementations
    │   │   └───EventStrategies           -- base for scheduled event implementations
    │   │           └───DateTimeStrategies        -- base for strategies triggered by datetime conditions
    │   │           └───EnvironmentStrategies     -- base for strategies triggered by environment state conditions
    │   │           └───SubscriptionStrategies    -- base for strategies which must be subscribed too before affecing a user
    │   └───Telegram                      -- telegram event consumption service 
    │       ├───Extensions
    │       ├───Options
    │       └───Services
    └───BlueAndromeda.Api             -- GenericHost service build and hosting logic
        ├───Controllers                   -- REST API endpoint logic implementation
        ├───Extensions
        └───Properties
```

<div align="right">(<a href="#readme-top">back to top</a>)</div>

<!-- CONTRIBUTING -->
## Contributing

SOURCE NOT YET PUBLISHED

<!--
See the [open issues](https://github.com/alyssa.yours/blueandromeda/issues) for a full list of proposed features (and known issues).

Contributions are what make the open source community such an amazing place to learn, inspire, and create. Any contributions you make are **greatly appreciated**.

If you have a suggestion that would make this better, please fork the repo and create a pull request. You can also simply open an issue with the tag "enhancement".
Don't forget to give the project a star! Thanks again!

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request

<div align="right">(<a href="#readme-top">back to top</a>)</div>
-->


<!-- LICENSE -->
## License


SOURCE NOT YET PUBLISHED

<!-- Distributed under the MIT License. See `LICENSE.txt` for more information.-->

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- CONTACT -->
## Contact

Product Owner:
- <a href="https://t.me/alyssa_yours">Telegram - Alyssa</a>
 - Discord - alyssa.yours_84301

Project Link: [https://gitlab.com/alyssa.yours/blueandromeda](https://github.com/alyssa.yours/blueandromeda)

<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- ACKNOWLEDGMENTS -->
## Acknowledgments

* [PiShock.com](https://www.pishock.com/) provided devices and API service <br/><em>(this project is third party and not officially affiliated with PiShock.com)</em>


<div align="right">(<a href="#readme-top">back to top</a>)</div>



<!-- MARKDOWN LINKS & IMAGES -->
<!-- https://www.markdownguide.org/basic-syntax/#reference-style-links -->
[contributors-shield]: https://gitlab.com/alyssa.yours/blueandromeda/badges/main/pipeline.svg
[contributors-url]: https://gitlab.com/alyssa.yours/blueandromeda/-/issues
[product-screenshot]: doc/images/screenshot.png
[project-header]: doc/images/project-header.png
[community-development-header]: doc/images/community-development-header.png

